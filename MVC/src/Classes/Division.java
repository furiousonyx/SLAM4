package Classes;

public class Division {

	private String code;
	private String libelle;
	

	/**
	 * constructeur par defaut
	 */
	public Division() {
		setCode("truc");
		setLibelle("bidule");
	}

	/**
	 * contructeur
	 * 
	 * @param code :definie le code de la division
	 *          
	 * @param nom: definie le nom
	 *            
	 * @param prenom:definie le prenom
	 *           
	 * @param datenaiss  :definie la date de naissance
	 *          
	 */
	public Division(String code, String libelle) {
		this.code = code;
		this.libelle =libelle;

	}
	/* accesseur */

	/**
	 * 
	 * @return code
	 */

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 
	 * @return libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	/**
	 * methode toString pour retourner une chaine de caractere avec les attribut libelle et code
	 */
	@Override
	public String toString() {
		return " le code est : " + code + " avec le libelle " + libelle;
	}
	
}
