package Classes;

public class Eleve {

	private String code;
	private String nom;
	private String prenom;
	private String datenaiss;
	private Division laDivision;  


	
	/**
	 * constructeur par defaut
	 */
	public Eleve() {
		setCode("kuja");
		setNom("Hancock");
		setPrenom("Boa");
		setDatenaiss("20/05/1995");
		setLaDivision(null);
	}

	/**
	 * contructeur
	 * 
	 * @param code definie le code de la division
	 *          
	 * @param nom definie le nom
	 *            
	 * @param prenom definie le prenom
	 *           
	 * @param datenaiss definie la date de naissance
	 * @param laDivision 
	 *          
	 */
	public Eleve(String code, String nom, String prenom, String datenaiss) {
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.datenaiss = datenaiss;
	}
	/**
	 * contructeur
	 * 
	 * @param code definie le code de la division
	 *          
	 * @param nom definie le nom
	 *            
	 * @param prenom definie le prenom
	 *           
	 * @param datenaiss definie la date de naissance
	 * @param laDivision 
	 *          
	 */
	public Eleve(String code, String nom, String prenom, String datenaiss, Division laDivision) {
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.datenaiss = datenaiss;
		this.laDivision = laDivision;

	}
	/* accesseur */

	/**
	 * retourne le code
	 * @return code
	 */

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * retourne le nom
	 * @return nom
	 */
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * retourne le prenom
	 * @return prenom
	 */
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * retourne la date de naissance
	 * @return datenaiss
	 */
	
	public String getDatenaiss() {
		return datenaiss;
	}

	public void setDatenaiss(String datenaiss) {
		this.datenaiss = datenaiss;
	}
	/**
	 * 
	 * @return la division
	 */
	public Division getLaDivision() {
		return laDivision;
	}

	public void setLaDivision(Division laDivision) {
		this.laDivision = laDivision;
	}

	@Override
	public String toString() {
		return " le nom de l'eleve est : " + nom + " et son prenom " + prenom + " née le "+ datenaiss + " et a pour code " + code;
	}
	

}
