<?php

/**
 * @Entity @Table (name = "categorie")
 * */
class categorie {

    /**
     * @Id @Column(length=5)
     */
    private $codecateg;

    /**
     * @Column(length=2)
     * */
    private $nomcateg;

    /**
     * @Column(length=30)
     */
    function __construct() {
        $this->codecateg = "";
        $this->nomcateg = "";
    }

    function init($codecateg, $nomcateg) {
        $this->codecateg = $codecateg;
        $this->nomcateg = $nomcateg;
    }

    function get_codecateg() {
        return $this->codecateg;
    }

    function set_codecateg($codecateg) {
        $this->codecateg = $codecateg;
    }

    function get_nomcateg() {
        return $this->nomcateg;
    }

    function set_nomcateg($nomcateg) {
        $this->nomcateg = $nomcateg;
    }

}

?>