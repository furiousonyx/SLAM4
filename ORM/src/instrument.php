<?php

/**
 * @Entity @Table(name="instrument")
 * */
class Instrument {

    /**
     * @Id @Column(length = 5)
     * */
    private $ref;

    /**
     * @Column(length=30)
     */
    private $nom;

    /**
     * @Column(length=20)
     */
    private $marques;

    /**
     * @Column(length=150)
     */
    private $caract;

    /**
     * @Column(type="integer")
     */
    private $prix;

    /**
     * @Column(length=20)
     */
    private $photo;

    /**
     * @ManyToOne(targetEntity = "categorie")
     * @JoinColumn (name ="codecateg" , referencedColumnName="codecateg") 
     */
    private $codecateg;

    function __construct() {
        $this->ref = "";
        $this->nom = "";
        $this->marques = "";
        $this->caract = "";
        $this->prix = "";
        $this->photo = "";
        $this->codecateg = "";
    }

    function init($ref, $nom, $marques, $caract, $prix, $photo, $codecateg) {
        $this->ref = $ref;
        $this->nom = $nom;
        $this->marques = $marques;
        $this->caract = $caract;
        $this->prix = $prix;
        $this->photo = $photo;
        $this->codecateg = $codecateg;
    }

    function getRef() {
        return $this->ref;
    }

    function setRef($ref) {
        $this->ref = $ref;
    }

    function getNom() {
        return $this->nom;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function getMarques() {
        return $this->marques;
    }

    function setMarques($marques) {
        $this->marques = $marques;
    }

    function getCaract() {
        return $this->caract;
    }

    function setCaract($caract) {
        $this->caract = $caract;
    }

    function getPrix() {
        return $this->prix;
    }

    function setPrix($prix) {
        $this->prix = $prix;
    }

    function getPhoto() {
        return $this->photo;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }

}
