package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class connexionPsql {

	public static Connection connect;

	
	/**
	 * sert a la connection de posgresql
	 * @return connect
	 */
	public static Connection getInstance() {

		if (connect == null) {

			try {
				String url = "jdbc:postgresql://postgresql.bts-malraux72.net/amarques";
				connect = DriverManager.getConnection(url, "a.marques", "P@ssword");

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return connect;
	}

}
