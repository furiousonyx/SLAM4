<?php

/* cinemaconsultationBundle:Default:unfilm.html.twig */
class __TwigTemplate_c79c09d822be89187dac882045bd0022 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'paneltitle' => array($this, 'block_paneltitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "   Film : ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "titre"), "html", null, true);
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
";
    }

    // line 5
    public function block_paneltitle($context, array $blocks = array())
    {
        echo " <h3>Détail du film N°";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "id"), "html", null, true);
        echo "</h3> ";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "    <table class=\"table table-striped\">
        <tr>
            <th>Titre</th>
            <th>Resume</th>
            <th>Durée</th>
            <th>Pays</th>
            <th>Recettes</th>
        </tr>
        <tr>
            <th>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "titre"), "html", null, true);
        echo " </th>
            <td>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "resume"), "html", null, true);
        echo " </td>
            <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "duree"), "html", null, true);
        echo " </td>
            <td>";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "pays"), "html", null, true);
        echo " </td>
            <td>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "recettes"), "html", null, true);
        echo " </td>
        </tr>
    </table>
    <p>
        <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_homepage"), "html", null, true);
        echo "\">
            <span class=\"glyphicon glyphicon-chevron-left\"></span>
            Retour à la liste</a>&nbsp;&nbsp;&nbsp;
        <a href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_modifierfilm", array("id" => $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "id"))), "html", null, true);
        echo "\">
            <span class=\"glyphicon glyphicon-edit\"></span>
            Modifier le film</a>&nbsp;&nbsp;&nbsp;
        <a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_supprimerfilm"), "html", null, true);
        echo "\">
            <span class=\"glyphicon glyphicon-trash\"></span>
            Supprimer le film</a>&nbsp;&nbsp;&nbsp;
    </p>
";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Default:unfilm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 30,  93 => 27,  87 => 24,  80 => 20,  76 => 19,  72 => 18,  68 => 17,  64 => 16,  53 => 7,  50 => 6,  42 => 5,  33 => 3,  30 => 2,);
    }
}
