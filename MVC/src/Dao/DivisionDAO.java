package Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Classes.Division;

public class DivisionDAO extends DAO<Division> {

	/**
	 * execute la requete inserer des valeur a la table division
	 */
	public void create(Division obj) {
		try {
			PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO \"MVC\".division VALUES(?, ?)");
			prepare.setString(1, obj.getCode());
			prepare.setString(2, obj.getLibelle());

			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * execute la requete pour afficher la table division
	 * 
	 * @return la division
	 */
	public Division read(String code) {
		Division laDivision = new Division();
		try {
			ResultSet result = this.connect
					.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery("SELECT * FROM \"MVC\".division WHERE code = '" + code + "'");
			if (result.first())
				laDivision = new Division(code, result.getString("libelle"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return laDivision;
	}

	/**
	 * execute la requete pour mettre a jour la table Division
	 * 
	 */
	public void update(Division obj) {
		try {

			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"MVC\".division SET libelle = '" + obj.getLibelle() + "'"
							+ " WHERE code = '" + obj.getCode() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * execute la requete pour effacer la table division
	 */
	public void delete(Division obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"MVC\".division WHERE code = '" + obj.getCode() + "'");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
