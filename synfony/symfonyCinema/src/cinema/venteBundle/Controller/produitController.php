<?php

namespace cinema\venteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use cinema\venteBundle\Entity\produit;
use cinema\venteBundle\Form\produitType;

/**
 * produit controller.
 *
 */
class produitController extends Controller
{
    /**
     * Lists all produit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('cinemaventeBundle:produit')->findAll();

        return $this->render('cinemaventeBundle:produit:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new produit entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new produit();
        $form = $this->createForm(new produitType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('produit_show', array('id' => $entity->getId())));
        }

        return $this->render('cinemaventeBundle:produit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new produit entity.
     *
     */
    public function newAction()
    {
        $entity = new produit();
        $form   = $this->createForm(new produitType(), $entity);

        return $this->render('cinemaventeBundle:produit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a produit entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find produit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:produit:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing produit entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find produit entity.');
        }

        $editForm = $this->createForm(new produitType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:produit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing produit entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:produit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find produit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new produitType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('produit_edit', array('id' => $id)));
        }

        return $this->render('cinemaventeBundle:produit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a produit entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('cinemaventeBundle:produit')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find produit entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('produit'));
    }

    /**
     * Creates a form to delete a produit entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
