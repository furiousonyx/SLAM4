package Version_2;

import java.util.ArrayList;
import java.util.List;

public class Athlete extends Epreuve implements Comparable<Athlete> {

	private String nom;
	private String prenom;
	private List<Epreuve> lesEpreuve = new ArrayList<Epreuve>();
	private Integer nbPoints;
	private int rang;
	private int numEpreuve = 0;
	private static int cpt = 0;
	private boolean disqualifier,abandon;

	/**
	 * contructeur par defaut
	 */
	public Athlete() {
		cpt++;
		nbPoints=0;
		setNom("Jewelry");
		setPrenom("Bonney");
		setRang(cpt);
		lesEpreuve.add(new Epreuve("Escrime"));
		lesEpreuve.add(new Epreuve("Natation"));
		lesEpreuve.add(new Epreuve("Equitation"));
		lesEpreuve.add(new Epreuve("Course / Tir"));
		disqualifier =false ;
		abandon = false;
	}

	
	/**
	 * 
	 * @param nom : valeur du nom de l'athlete
	 * @param Prenom : valeur du prénom de l'athlete
	 */
	public Athlete(String nom, String Prenom ){
		cpt++;
		this.nom = nom;
		this.prenom = Prenom;
		this.setRang(cpt);
		nbPoints=0;
		lesEpreuve.add(new Epreuve("Escrime"));
		lesEpreuve.add(new Epreuve("Natation"));
		lesEpreuve.add(new Epreuve("Equitation"));
		lesEpreuve.add(new Epreuve("Course / Tir"));
		disqualifier =false ;
		abandon = false;
	}

	/**
	 * Contructeur
	 * 
	 * 
	 */
	public Athlete(String nom, String Prenom,int nbPoints) {
		cpt++;
		this.nom = nom;
		this.prenom = Prenom;
		this.setRang(cpt);
		this.nbPoints=nbPoints;
		lesEpreuve.add(new Epreuve("Escrime"));
		lesEpreuve.add(new Epreuve("Natation"));
		lesEpreuve.add(new Epreuve("Equitation"));
		lesEpreuve.add(new Epreuve("Course / Tir"));
		disqualifier =false ;
		abandon = false;
	}

	
	/**
	 * accesseur
	 * 
	 * @return nom de l'athlete
	 */

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * accesseur
	 * 
	 * @return prenom de l'athlete
	 */
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * accesseur
	 * 
	 * @return le nom des épreuve
	 */
	public List<Epreuve> getLesEpreuve() {
		return lesEpreuve;
	}

	/**
	 * 
	 * @param lesEpreuve
	 */
	public void setLesEpreuve(List<Epreuve> lesEpreuve) {
		this.lesEpreuve = lesEpreuve;
	}

	public Integer getNbPoints() {
		return nbPoints;
	}

	public void setNbPoints(Integer nbPoints) {
		this.nbPoints = nbPoints;
	}

	/**
	 * sert a ajouter le score
	 */

	public void ajouterScore(int point) {
		String[] epreuve = { "Escrime", "Natation", "Equitation", "Course/Tir" };
		nbPoints = point;
		lesEpreuve.add(new Epreuve(epreuve[numEpreuve], nbPoints));
	}

	/**
	 * Affiche les epreuves et les points obtenus
	 */
	public String toString() {
		String str = " ";
		for (Epreuve epreuves : lesEpreuve) {
			str = "Fin de l'épreuve " + epreuves.getLibelle() + " " + nom + " " + prenom + " a " + epreuves.getPoints()
					+ " points.";
		}
		return str;
	}

	/**
	 * Affiche les epreuves et les points obtenus
	 */
	public String toString2() {
		 
		return prenom+","+nom+","+nbPoints;
	}

	/**
	 * Affiche le nom de l'epreuve dans laquelle il se situe
	 * 
	 * @return str nom de l'epreuve
	 */
	public String AfficherEpreuve() {
		String str = " ";

		str = lesEpreuve.get(numEpreuve).toString();

		return str;

	}

	/**
	 * Compte le score au total
	 */
	public void CompteScore() {
		nbPoints = 0;
		for (Epreuve ep : lesEpreuve) {
			nbPoints += ep.getPoints();
		}
	}

	/**
	 * Change le numero d'epreuves
	 */
	public void ChangerEpreuve() {
		numEpreuve++;
	}

	/**
	 * Affiche les scores pour chaque epreuves
	 * 
	 * @return str
	 */
	public String AfficheScore(Athlete a) {
		String str = a.getPrenom() + " " + a.getNom() +"  a obtenu : \n";
		
		for (Epreuve epreuves : a.getLesEpreuve()) {
			int nb = epreuves.getPoints();
			str += epreuves.getLibelle() + " -> " + epreuves.getPoints()+" points.\n";
		}
		return str;
	}

	/**
	 * sert a trier les rang des athlete
	 */

	@Override
	public int compareTo(Athlete ath) {

		return this.nbPoints.compareTo(ath.nbPoints);
	}
 
	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}

	public boolean getDisqualifier() {
		return disqualifier;
	}

	public void setDisqualifier(boolean disqualifier) {
		this.disqualifier = disqualifier;
	}

	public boolean getAbandon() {
		return abandon;
	}

	public void setAbandon(boolean abandon) {
		this.abandon = abandon;
	}

}
