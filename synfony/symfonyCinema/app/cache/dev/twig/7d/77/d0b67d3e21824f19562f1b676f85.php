<?php

/* cinemaconsultationBundle:Default:index.html.twig */
class __TwigTemplate_7d77d0b67d3e21824f19562f1b676f85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    affiche un films
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_homepage"), "html", null, true);
        echo "\">
        Accueil


    </a>


    <h2>Détails du film ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "titre"), "html", null, true);
        echo " </h2><br>
    <p>
        ";
        // line 18
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 19
            echo "        <p> ";
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "
        </p>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 22
        echo "</p>


<h1> Titre : ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "titre"), "html", null, true);
        echo "</h1>
<p>Resume : ";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "resume"), "html", null, true);
        echo "</p>
<p>Duree : ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "duree"), "html", null, true);
        echo " minutes</p>
<p> Pays : ";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "pays"), "html", null, true);
        echo "</p>
<p> Recette : ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "recettes"), "html", null, true);
        echo " yen </p>

";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 29,  90 => 28,  86 => 27,  82 => 26,  78 => 25,  73 => 22,  63 => 19,  58 => 18,  53 => 15,  43 => 8,  40 => 7,  37 => 6,  32 => 3,  29 => 2,);
    }
}
