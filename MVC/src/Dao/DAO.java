package Dao;

import java.sql.Connection;

public abstract class DAO<T>{
	public Connection connect = connexionPsql.getInstance();
	
	/**
	 *
	 * @param obj
	 */
	public abstract void create(T obj);
	/**
	 * 
	 * @param code
	 */
	public abstract Object read(String code);
	/**
	 * 
	 * @param obj
	 */
	public abstract void update(T obj);
	/**
	 * 
	 * @param obj
	 */
	public abstract void delete(T obj);
}