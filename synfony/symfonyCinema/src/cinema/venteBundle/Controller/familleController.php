<?php

namespace cinema\venteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use cinema\venteBundle\Entity\famille;
use cinema\venteBundle\Form\familleType;

/**
 * famille controller.
 *
 */
class familleController extends Controller
{
    /**
     * Lists all famille entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('cinemaventeBundle:famille')->findAll();

        return $this->render('cinemaventeBundle:famille:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new famille entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new famille();
        $form = $this->createForm(new familleType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('famille_show', array('id' => $entity->getId())));
        }

        return $this->render('cinemaventeBundle:famille:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new famille entity.
     *
     */
    public function newAction()
    {
        $entity = new famille();
        $form   = $this->createForm(new familleType(), $entity);

        return $this->render('cinemaventeBundle:famille:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a famille entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:famille')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find famille entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:famille:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing famille entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:famille')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find famille entity.');
        }

        $editForm = $this->createForm(new familleType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cinemaventeBundle:famille:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing famille entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cinemaventeBundle:famille')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find famille entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new familleType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('famille_edit', array('id' => $id)));
        }

        return $this->render('cinemaventeBundle:famille:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a famille entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('cinemaventeBundle:famille')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find famille entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('famille'));
    }

    /**
     * Creates a form to delete a famille entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
