package Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Dao.DivisionDAO;

import Classes.Eleve;;

public class EleveDAO extends DAO<Eleve> {

	/**
	 * execute la requete inserer des valeur a la table eleve
	 */
	public void create(Eleve obj) {
		DivisionDAO div = new DivisionDAO();
		if (div.read(obj.getLaDivision().getCode()).getCode().equals("null")) {
			System.out.println("code division : " + obj.getLaDivision().getCode());
			div.create(obj.getLaDivision());
		}

		try {
			PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO \"MVC\".eleve VALUES(?, ?,?,?,?)");
			prepare.setString(1, obj.getCode());
			prepare.setString(2, obj.getNom());
			prepare.setString(3, obj.getPrenom());
			prepare.setString(4, obj.getDatenaiss());
			prepare.setString(5, obj.getLaDivision().getCode());

			prepare.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * execute la requete pour afficher la table division
	 * 
	 * @return l'eleve
	 */
	public Eleve read(String code) {
		Eleve eleve = new Eleve();
		DivisionDAO Divd = new DivisionDAO();
		try {
			ResultSet result = this.connect
					.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeQuery("SELECT * FROM \"MVC\".eleve WHERE code = '" + code + "'");
			if (result.first())
				eleve = new Eleve(code, result.getString("nom"), result.getString("prenom"),
						result.getString("datenaiss"));
			//eleve.setLaDivision(Divd.read(result.getString("codediv")));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return eleve;
	}

	/**
	 * execute la requete pour mettre a jour la table Division
	 * 
	 */
	public void update(Eleve obj) {
		try {

			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("UPDATE \"MVC\".eleve SET nom = "+obj.getNom()+"' , prenom='"+ obj.getPrenom()+"'  , datenaiss='"+obj.getDatenaiss()+"',  codediv='"+obj.getLaDivision().getCode()+"'"
							+ " WHERE code = '" + obj.getCode() + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * execute la requete pour effacer la table division
	 */
	public void delete(Eleve obj) {
		try {
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
					.executeUpdate("DELETE FROM \"MVC\".division WHERE code = '" + obj.getCode() + "'");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
