<!doctype html>
<html>
	<head>
		<link rel="stylesheet" type="text/css"  href="CSS/sup1200.css"/>

		<script>
			function affiche(nom, nomPhoto, marque, caract, prix, categ)
			{
				document.getElementById("titre").innerHTML=nom;
				document.getElementById("imageInstrument").src=nomPhoto;
				document.getElementById("marque").innerHTML=marque;
				document.getElementById("caract").innerHTML=caract;
				document.getElementById("prix").innerHTML=prix+" €";
				document.getElementById("categ").innerHTML=categ;
			}
		</script>

	</head>
  
	<body>
		<div id="photos">
			<!-- Cette balise h1 avec un espace sert à créer une ligne d'espace à gauche du nom de l'instrument s'affichant dans la partie droite de la page -->
			<h1>&nbsp;</h1>

			<?php
                        require "./bootstrap.php";
                         
                        // lecture de tous les instruments de la table Instruments
                           $instruments = $entityManager->getRepository('instrument')->findAll();

                           // - Mettre ici une BOUCLE DE LECTURE DE TOUS LES INSTRUMENTS CONTENANT L'AFFICHAGE DE LA PHOTO DE L'INSTRUMENT LU
			foreach ($instruments as $instrument)
                        {
                            echo "<div class='cellule'>";
                            echo "<img class='vignette' src='Images/".$instrument->getPhoto();
                            echo "' onclick=\"affiche('".$instrument->getNom().
                                    "','Images/".$instrument->getPhoto()."','".$instrument->getMarque()
                                    ."','".utf8_encode($instrument->getCaracteristiq())."',".$instrument->getPrix()
                                    .",'".utf8_encode($instrument->getLaCategorie()->getNom_categ())."')\"/></br>".$instrument->getNom()."</div>";
                        }               
			// - Chaque photo d'instrument devra être affichée dans une div appartenant à la classe "cellule"
			//   et la balise <img> devra appartenir à la classe "vignette"
			//
			// - Le clic sur l'image devra appeler la fonction JavaScript "affiche" en lui passant en paramètres 
			//   le nom de l'instrument, le nom de son fichier photo, sa marque, ses caractéristiques, son prix, et le libellé de sa catégorie
			?>
		</div>
    

		<div id="instrument">
			<h1>   
				<span id="titre">
					<?php
					//Mettre ici l'affichage du nom du premier instrument trouvé dans la table
					// (C'est cet instrument qui est affiché par défaut à l'ouverture de la page)
                                        echo $instruments[0]->getNom();
					?>
				</span>
			</h1>
				
			<img id="imageInstrument" src="Images/
				<?php
					//Mettre ici l'affichage de la photo du premier instrument trouvé dans la table
					// (C'est cet instrument qui est affiché par défaut à l'ouverture de la page)
                                        echo $instruments[0]->getPhoto();
				?>
			"/>

			<hr/>

			<table  id="fichedetail">
				<tr>
					<th colspan="2" id="titre_bloc">Fiche détaillée</th>
				</tr>
				<tr>
					<th>Marque</th>
					<td>
						<span id="marque">
							<?php
								//Mettre ici l'affichage de la marque du premier instrument trouvé dans la table
								// (C'est cet instrument qui est affiché par défaut à l'ouverture de la page)
                                                                echo $instruments[0]->getMarque();
							?>
						</span>
					</td>
				</tr>
				<tr>
                                    <th>Caract&eacute;ristiques</th>
					<td>
						<span id="caract">
							<?php
								//Mettre ici l'affichage des caractéristiques du premier instrument trouvé dans la table
								// (C'est cet instrument qui est affiché par défaut à l'ouverture de la page)
                                                                echo $instruments[0]->getCaracteristiq();
							?>
						</span>
					</td>
				</tr>
				<tr>
					<th>Prix</th>
					<td>
						<span id="prix">
							<?php
								//Mettre ici l'affichage du prix du premier instrument trouvé dans la table
								// (C'est cet instrument qui est affiché par défaut à l'ouverture de la page)
                                                                echo $instruments[0]->getPrix();
							?> €
						</span>
					</td>
				</tr>
				<tr>
                                    <th>Cat&eacute;gorie</th>
					<td>
						<span id="categ">
							<?php
								//Mettre ici l'affichage du nom de la catégorie du premier instrument trouvé dans la table
								// (C'est cet instrument qui est affiché par défaut à l'ouverture de la page)
                                                                echo $instruments[0]->getLaCategorie()->getNom_categ();
							?>
						</span>
					</td>
				</tr>

			</table>
		</div>

	</body>
</html>