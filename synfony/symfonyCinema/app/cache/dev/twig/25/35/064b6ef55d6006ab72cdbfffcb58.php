<?php

/* cinemaconsultationBundle:Default:listefilm.html.twig */
class __TwigTemplate_2535064b6ef55d6006ab72cdbfffcb58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    affiche un films
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "    <table border=\"1\">
        ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["film"]) ? $context["film"] : $this->getContext($context, "film")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["unFilm"]) {
            // line 9
            echo "            <tr>
                <th>Titre : ";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "titre"), "html", null, true);
            echo "</th>
                <td>Resume : ";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "resume"), "html", null, true);
            echo "</td>
                <td>Duree : ";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "duree"), "html", null, true);
            echo " minutes </td>
                <td>Pays : ";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "pays"), "html", null, true);
            echo " }</td>
                <td>Recette : ";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["unFilm"]) ? $context["unFilm"] : $this->getContext($context, "unFilm")), "recettes"), "html", null, true);
            echo " yen </td> 
            </tr>

        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 18
            echo "            Erreur
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unFilm'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 20
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Default:listefilm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 20,  76 => 18,  67 => 14,  63 => 13,  59 => 12,  55 => 11,  51 => 10,  48 => 9,  43 => 8,  40 => 7,  37 => 6,  32 => 3,  29 => 2,);
    }
}
