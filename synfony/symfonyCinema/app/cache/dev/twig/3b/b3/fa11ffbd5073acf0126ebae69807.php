<?php

/* cinemaconsultationBundle:Film:ajouter.html.twig */
class __TwigTemplate_3bb3fa11ffbd5073acf0126ebae69807 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'paneltitle' => array($this, 'block_paneltitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " Ajout film - ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
    }

    // line 3
    public function block_paneltitle($context, array $blocks = array())
    {
        echo " <h3> Ajouter un film</h3> ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"row\">
        <div class=\"col-sm-6\">
            <table class=\"table table-striped\">
                <tr>
                    <td>

                        <form class=\"form-horizontal\" action=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_ajouter"), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">
                            <span class=\"alert-danger\">
                                ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                            </span>

                            <span class=\"alert-danger\"> ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'errors');
        echo " </span>
                            <div class=\"form-group\">
                                <div class=\"col-sm-2 control-label\">
                                    ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'label');
        echo "
                                </div>
                                <div class=\"col-sm-3\">
                                    ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                </div>
                            </div>

                            ";
        // line 26
        echo "       
                            <div class=\"form-group\">
                                <div class=\"col-sm-2 control-label\">
                                    ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duree"), 'label');
        echo "
                                </div>                          
                                <div class=\"col-sm-3\">
                                    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duree"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                </div>
                            </div>

                            ";
        // line 36
        echo "  
                            <div class=\"form-group\">
                                <div class=\"col-sm-2 control-label\">
                                    ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays"), 'label');
        echo "
                                </div>
                                <div class=\"col-sm-3\">
                                    ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                </div>
                            </div>
                            ";
        // line 46
        echo "                            <div class=\"form-group\">  
                                <div class=\"col-sm-2 control-label\">
                                    ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "recettes"), 'label');
        echo "
                                </div>
                                <div class=\"col-sm-3\">
                                    ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "recettes"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                </div>
                            </div>
                            ";
        // line 54
        echo "    
                            <div class=\"form-group\">                                
                                <div class=\"col-sm-2 control-label\">
                                    ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "resume"), 'label');
        echo "
                                </div>
                                <div class=\"col-sm-3\">
                                    ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "resume"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                </div>
                            </div>
                            ";
        // line 63
        echo "    
                            <div class=\"form-group\">
                                <div class=\"col-sm-offset-2 col-sm-4\">
                                    <input type=\"submit\" class=\"btn btn-primary\" value=\"Valider\"/>
                                </div>
                            </div>
                            ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "  
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <br>
                            <a href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_homepage"), "html", null, true);
        echo "\">
                                <span class=\"glyphicon glyphicon-chevron-left\"></span>
                                Retour à la liste
                            </a>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Film:ajouter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 77,  163 => 69,  155 => 63,  149 => 60,  143 => 57,  138 => 54,  132 => 51,  126 => 48,  122 => 46,  116 => 42,  110 => 39,  105 => 36,  98 => 32,  92 => 29,  87 => 26,  80 => 22,  74 => 19,  68 => 16,  62 => 13,  55 => 11,  47 => 5,  44 => 4,  38 => 3,  30 => 2,);
    }
}
