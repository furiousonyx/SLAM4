package tests;

import Classes.Division;
import Classes.Eleve;
import Dao.DAO;
import Dao.EleveDAO;
import Dao.DivisionDAO;

public class testDAO {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DAO<Eleve> eleve = new EleveDAO();
		
		DAO<Division> division = new DivisionDAO();

		Division uneDivision = new Division("blutdtu", "Cours");

		
		// test de la recherche d'un élève en fonction de son code
		System.out.println(eleve.read("blabla").toString());
		
		Eleve unEleve = new Eleve("t2","truc","bidule","14/05/2001", uneDivision);
		
		// test de l'insertion d'un nouvel élève
		eleve.create(unEleve);
		
		// test de la recherche d'un élève en fonction de son code

		System.out.println(eleve.read("t4").toString());
	
		
		//test de la modification d'un eleve
		
		//eleve.update(unEleve);
		
		
		// test de la suppression d'un élève
		eleve.delete(unEleve);
		division.delete(uneDivision);
	
		// On afficche les informatique de la classe qui a pour code XFSZA
				System.out.println(division.read("Cours"));
				division.create(uneDivision);

	}

}
