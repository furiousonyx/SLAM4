<?php

/* cinemaconsultationBundle:Film:modifier.html.twig */
class __TwigTemplate_075e5c22aa435fa57a99d7e28c2abb87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'paneltitle' => array($this, 'block_paneltitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " Modifier le film - ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
    }

    // line 3
    public function block_paneltitle($context, array $blocks = array())
    {
        echo " <h3> Modifier le film</h3> ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"row\">
        <div class=\"col-sm-6\">
            <table class=\"table table-striped\">
                <tr>
                    <td>
                        <form class=\"form-horizontal\" action=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_modifierfilm", array("id" => $this->getAttribute((isset($context["film"]) ? $context["film"] : $this->getContext($context, "film")), "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">
                           <span class=\"alert-danger\">
                               ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                           </span>
                           
                           <span class=\"alert-danger\"> ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'errors');
        echo " </span>
                           <div class=\"form-group\">
                               <div class=\"col-sm-2 control-label\">
                                   ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'label');
        echo "
                               </div>
                               <div class=\"col-sm-3\">
                                   ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "titre"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                               </div>
                           </div>
                            <span class=\"alert-danger\"> ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duree"), 'errors');
        echo " </span>
                           <div class=\"form-group\">
                               <div class=\"col-sm-2 control-label\">
                                   ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duree"), 'label');
        echo "
                               </div>
                               <div class=\"col-sm-3\">
                                   ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duree"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                               </div>
                           </div>
                            
                             <span class=\"alert-danger\"> ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays"), 'errors');
        echo " </span>
                           <div class=\"form-group\">
                               <div class=\"col-sm-2 control-label\">
                                   ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays"), 'label');
        echo "
                               </div>
                               <div class=\"col-sm-3\">
                                   ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "pays"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                               </div>
                           </div>
                                <span class=\"alert-danger\"> ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "resume"), 'errors');
        echo " </span>
                           <div class=\"form-group\">
                               <div class=\"col-sm-2 control-label\">
                                   ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "resume"), 'label');
        echo "
                               </div>
                               <div class=\"col-sm-15\">
                                   ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "resume"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                               </div>
                           </div>
                                <span class=\"alert-danger\"> ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "recettes"), 'errors');
        echo " </span>
                           <div class=\"form-group\">
                               <div class=\"col-sm-2 control-label\">
                                   ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "recettes"), 'label');
        echo "
                               </div>
                               <div class=\"col-sm-3\">
                                   ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "recettes"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                               </div>
                           </div>
                            
                           
                          <div class=\"form-group\">
                              <div class=\"col-sm-offset-2 col-sm-4\">
                                   <input type=\"submit\" class=\"btn btn-primary\" value=\"Enregistrer\"/>
                              </div>
                          </div>
                          ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
                        </form> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><br>
                            <a href=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_homepage"), "html", null, true);
        echo "\">
                                <span class=\"glyphicon glyphicon-chevron-left\"></span>
                                Retour à la liste
                            </a>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "cinemaconsultationBundle:Film:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 75,  165 => 68,  152 => 58,  146 => 55,  140 => 52,  134 => 49,  128 => 46,  122 => 43,  116 => 40,  110 => 37,  104 => 34,  97 => 30,  91 => 27,  85 => 24,  79 => 21,  73 => 18,  67 => 15,  61 => 12,  54 => 10,  47 => 5,  44 => 4,  38 => 3,  30 => 2,);
    }
}
