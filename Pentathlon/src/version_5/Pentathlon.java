package version_5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

import Version_2.Athlete;
import javax.swing.JRadioButton;

/**
 * 
 * @author Alexandre Marques
 *
 */

public class Pentathlon extends JFrame {

	private JPanel contentPane;
	private JTextField Txtnom;
	private JTextField txtPrenom;
	private JTextField txtPoint;
	JLabel lblScoreEp; 
	private JTextField txtEpreuve;
	private JRadioButton rdbtnOui;
	private JRadioButton rdbtnNon;
	JLabel lblDisqualifierAbandon;
	JLabel lblEpreuve;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pentathlon frame = new Pentathlon();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Pentathlon() {
		setTitle("Pentathlon");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 517, 467);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Txtnom = new JTextField();
		Txtnom.setBounds(70, 77, 114, 19);
		contentPane.add(Txtnom);
		Txtnom.setColumns(10);
		
		txtPrenom = new JTextField();
		txtPrenom.setBounds(325, 77, 114, 19);
		txtPrenom.setColumns(10);
		contentPane.add(txtPrenom);
		
		JLabel lblNom = new JLabel("Nom");
		lblNom.setBounds(12, 79, 70, 15);
		contentPane.add(lblNom);
		
		JLabel lblPrenom = new JLabel("Prenom");
		lblPrenom.setBounds(237, 79, 70, 15);
		contentPane.add(lblPrenom);
				
		txtPoint = new JTextField();
		txtPoint.setBounds(168, 190, 114, 19);
		contentPane.add(txtPoint);
		txtPoint.setColumns(10);
		txtPoint.setVisible(false);
		
		lblScoreEp = new JLabel("score de l'epreuve");
		lblScoreEp.setBounds(24, 192, 131, 19);
		contentPane.add(lblScoreEp);
		lblScoreEp.setVisible(false);
		
		txtEpreuve = new JTextField();
		txtEpreuve.setBounds(168, 12, 114, 19);
		contentPane.add(txtEpreuve);
		txtEpreuve.setColumns(10);
		
		JLabel lblEpreuve = new JLabel("Epreuve");
		lblEpreuve.setBounds(70, 14, 70, 15);
		contentPane.add(lblEpreuve);
		
		
		JRadioButton rdbtnOui = new JRadioButton("oui");
		rdbtnOui.setBounds(57, 137, 149, 23);
		contentPane.add(rdbtnOui);
		rdbtnOui.setVisible(false);
		rdbtnOui.isSelected();
		
		lblDisqualifierAbandon = new JLabel("Disqualifier/abandon");
		lblDisqualifierAbandon.setBounds(137, 108, 205, 21);
		contentPane.add(lblDisqualifierAbandon);
		lblDisqualifierAbandon.setVisible(false);
		
		rdbtnNon = new JRadioButton("non");
		rdbtnNon.setBounds(236, 137, 149, 23);
		contentPane.add(rdbtnNon);
		rdbtnNon.setVisible(false);
		rdbtnNon.isSelected();
		
		
		
		
		JButton btnValider = new JButton("valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 lblEpreuve.setVisible(false);
				 System.out.println (txtEpreuve.getText ());
				 txtEpreuve.setVisible(false);
				 lblNom.setVisible(false);
				 lblPrenom.setVisible(false);
				 System.out.println (txtPrenom.getText());
				 System.out.println (Txtnom.getText());
				 Txtnom.setVisible(false);
				 txtPrenom.setVisible(false);
				 txtPoint.setVisible(true);
				 lblScoreEp.setVisible(true);
				 lblDisqualifierAbandon.setVisible(true);
				 rdbtnOui.setVisible(true);
				 rdbtnNon.setVisible(true);
				 
			}
			
		});
		btnValider.setBounds(168, 314, 117, 25);
		contentPane.add(btnValider);
		
	}
}
