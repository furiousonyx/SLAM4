package Version_2;

public class Epreuve {

	private String libelle;
	private int nbpoints;

	/**
	 * Constructeur par defaut
	 * 
	 */
	public Epreuve() {
		setLibelle("");
		setPoints(0);
	}

	/**
	 * contructeur
	 */
	public Epreuve(String libelle) {
		this.setLibelle(libelle);
	}
	public Epreuve(String libelle, int nbpoints) {
		this.libelle = libelle;
		this.nbpoints = nbpoints;
	}

	/** 
	 * accesseur
	 */

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getPoints() {
		return nbpoints;
	}

	public void setPoints(int points) {
		this.nbpoints = points;
	}

	/**
	 * Affichage du nom de l'epreuve et du nombre de points
	 */
	public String toString() {
		return "Epreuve  " + libelle;
	}

}
