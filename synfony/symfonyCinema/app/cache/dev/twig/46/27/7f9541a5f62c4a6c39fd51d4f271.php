<?php

/* ::base.html.twig */
class __TwigTemplate_46277f9541a5f62c4a6c39fd51d4f271 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'paneltitle' => array($this, 'block_paneltitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <style type='text/css'>
            body { padding-bottom: 70px; }
        </style>
    </head>
    <body>
        <div claas=\"wrap\">
            <div class=\"container\">
                <header class=\"page-header\">
                    <div class=\"row\">
                        <div class=\"col-sm-8\">
                            <div class=\"row\">
                                <h1>Gestion des films</h1>
                            </div>
                            <div class=\"row\">
                                <h4>Ce projet est ... </h4>
                            </div>
                        </div>
                        <div class=\"col-sm-4\">
                            <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/cinema.jpg"), "html", null, true);
        echo "\" >
                        </div>
                    </div>
                </header>
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <ul class=\"nav nav-pills\">
                            <li class =\"active\">
                                <a href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_homepage"), "html", null, true);
        echo "\">
                                    Accueil
                                </a>
                            </li>
                            <li class =\"active\">
                                <a href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_ajouter"), "html", null, true);
        echo "\">
                                    Ajouter un film
                                </a>
                            </li>
                            <li class =\"active\">
                                <a href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cinemaconsultation_affichelesfilms2"), "html", null, true);
        echo "\">
                                    Afficher les films
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                    <hr />
                    </div>
                </div>
                               
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <div class=\"panel panel-info\">
                            <div class=\"panel-heading\">
                                ";
        // line 62
        $this->displayBlock('paneltitle', $context, $blocks);
        // line 63
        echo "                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        ";
        // line 70
        $this->displayBlock('body', $context, $blocks);
        // line 71
        echo "                    </div>
                </div>
                <div id=\"push\"></div>
            </div>
        </div>
        <div class=\"nav navbar-fixed-bottom\">
            <div class=\"container\">
                <hr />
                <div id=\"footer\">
                    <p> STS2 - .....
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "AlloCiné!";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "            <link rel='stylesheet' href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\" type='text/css' />
        ";
    }

    // line 62
    public function block_paneltitle($context, array $blocks = array())
    {
        echo " ";
    }

    // line 70
    public function block_body($context, array $blocks = array())
    {
        echo " ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 70,  150 => 62,  143 => 7,  140 => 6,  134 => 5,  115 => 71,  113 => 70,  104 => 63,  102 => 62,  82 => 45,  74 => 40,  66 => 35,  35 => 9,  33 => 6,  23 => 1,  83 => 20,  76 => 18,  67 => 14,  63 => 13,  59 => 12,  55 => 27,  51 => 10,  48 => 9,  43 => 8,  40 => 7,  37 => 6,  32 => 3,  29 => 5,);
    }
}
