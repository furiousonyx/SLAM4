<?php

namespace cinema\venteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class produitType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text')
                ->add('description', 'text')
                ->add('prixHT', 'text')
                ->add('famille', 'entity', array(
                    'label'=>'Famille',
                    'class'=> 'cinemaventeBundle:famille',
                    'property'=>'libelle'
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'cinema\venteBundle\Entity\produit'
        ));
    }

    public function getName() {
        return 'cinema_ventebundle_produittype';
    }

}
